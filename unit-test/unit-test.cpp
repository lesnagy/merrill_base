#include <stdio.h>
#include <gtest/gtest.h>

#include <vector>

#include "add.h"
#include "mult.h"

/* this is a test that can pass*/
TEST(Application, addition) {
    AddFunction add;
    
    std::vector<float> a = { 1,2,3,4,5 };
    std::vector<float> b = { 2,2,2,2,2 };

    std::vector<float> exp_c = { 3,4,5,6,7 };
    std::vector<float> act_c = add(a, b);

    EXPECT_EQ(exp_c.size(), act_c.size());
    size_t len = exp_c.size();

    for (size_t i = 0; i < len; ++i) {
        EXPECT_EQ(exp_c[i], act_c[i]);
    }
}

/* this is a test that can pass*/
TEST(Application, multiplication) {
    MultFunction mult;

    std::vector<float> a = { 1,2,3,4,5 };
    std::vector<float> b = { 2,2,2,2,2 };

    std::vector<float> exp_c = { 2,4,6,8,10 };
    std::vector<float> act_c = mult(a, b);

    EXPECT_EQ(exp_c.size(), act_c.size());
    size_t len = exp_c.size();

    for (size_t i = 0; i < len; ++i) {
        EXPECT_EQ(exp_c[i], act_c[i]);
    }
}
