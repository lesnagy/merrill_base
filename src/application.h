#pragma once

#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#include <rapidxml_utils.hpp>

#include "add.h"
#include "mult.h"

// Forward declare AppConfig classs.
class AppConfig;

// Define a bunch of types.
using string = std::string;

using char_list = std::vector<char>;
using string_list = std::vector<std::string>;

using ptr_app_config = std::unique_ptr<AppConfig>;

using xml_doc = rapidxml::xml_document<>;
using xml_node = rapidxml::xml_node<>;

// Declare AppConfig
class AppConfig
{
public:
    AppConfig();

    inline const string& system_name() { return m_system_name; }
    inline const string& opencl_kernel_dir() { return m_opencl_kernel_dir; }
    inline const int platform_index() { return m_platform_index; }
    inline const int device_index() { return m_device_index; }

private:
    string m_system_name;
    string m_opencl_kernel_dir;
    int m_platform_index;
    int m_device_index;

    char_list get_xml_data();
};

// Declare App.
class App
{
public:
    App();

    AddFunction add_function();
    MultFunction mult_function();

private:
    ptr_app_config m_app_config;
    platform m_platform;
    device m_device;
    context m_context;
    command_queue m_command_queue;
    string_list m_kernel_sources;
    program_sources m_sources;
    program m_program;

    void init_config_data();
    void init_platform();
    platform_list get_platforms();
    void init_device();
    device_list get_devices();
    void init_context();
    context new_context();
    void init_command_queue();
    command_queue new_command_queue();
    void read_sources();
    void build_sources();
    program new_program();
};

// Application exception
class AppException: public std::runtime_error
{
public:
    explicit AppException(const char* message):
       runtime_error(message)  
    {}

    explicit AppException(const std::string& message):
       runtime_error(message)
    {}

    virtual ~AppException() throw() {}
};
