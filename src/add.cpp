#include "add.h"

AddFunction::AddFunction()
{}

std::vector<float> AddFunction::operator()(std::vector<float>& a, std::vector<float>& b)
{
    std::vector<float> output;
    
    if (a.size() == b.size()) {
        size_t len = a.size();
        for (size_t i = 0; i < len; ++i) {
            output.push_back(a[i] + b[i]);
        }
    } else {
        std::cout << "Error condition" << std::endl;
    }

    return output;
}

