#pragma once

#include <iostream>
#include <vector>

class AddFunction
{
public:
    AddFunction();
    std::vector<float> operator () (std::vector<float>& a, std::vector<float>& b);
};

