#include <vector>

#include "add.h"
#include "mult.h"

int main() {
    MultFunction mult; 
    AddFunction add;

    std::vector<float> a = { 1,2,3,4,5,6,7 };
    std::vector<float> b = { 2,2,2,2,2,2,4 };

    std::vector<float> result;

    result = mult(a, b);
    std::cout << "Result after multiplication: " << std::endl;
    for (const auto& v : result) {
        std::cout << v << std::endl;
    }

    result = add(a, b);
    std::cout << "Result after addition: " << std::endl;
    for (const auto& v : result) {
        std::cout << v << std::endl;
    }

    return 0;
}
