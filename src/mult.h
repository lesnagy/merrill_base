#pragma once

#include <iostream>
#include <vector>

class MultFunction
{
public:
    MultFunction();
    std::vector<float> operator () (std::vector<float> & a, std::vector<float> & b);
};
