# Sets up the Google-test library
# 
# GOOGLE_TEST_BUILT              - has google test been built
# GOOGLE_TEST_INCLUDES           - where to find 'gtest/gtest.h' etc
# GOOGLE_TEST_LIBRARY_gmock      - GoogleTest mock objects library
# GOOGLE_TEST_LIBRARY_gmock_main - GoogleTest mock objects library with main
# GOOGLE_TEST_LIBRARY_gtest      - GoogleTest library with main
# GOOGLE_TEST_LIBRARY_gtest_main - GoogleTest library with main

macro(display_google_test_info)
    message(STATUS "GOOGLE_TEST_INCLUDES: ${GOOGLE_TEST_INCLUDES}")
    message(STATUS "GOOGLE_TEST_LIBRARY_gmock: ${GOOGLE_TEST_LIBRARY_gmock}")
    message(STATUS "GOOGLE_TEST_LIBRARY_gmock_main: ${GOOGLE_TEST_LIBRARY_gmock_main}")
    message(STATUS "GOOGLE_TEST_LIBRARY_gtest: ${GOOGLE_TEST_LIBRARY_gtest}")
    message(STATUS "GOOGLE_TEST_LIBRARY_gtest_main: ${GOOGLE_TEST_LIBRARY_gtest_main}")
endmacro()

macro(setup_google_test)

  if (GOOGLE_TEST_BUILT)
    display_google_test_info()
    include_directories(${GOOGLE_TEST_INCLUDES})
  else ()
    # Set google test name
    set(GOOGLE_TEST "google-test"
        CACHE PATH "Google test directory name"
    )

    # Set google test version
    set(GOOGLE_TEST_VERSION "1.10.0"
        CACHE PATH "Google test version"
    )

    # Set google test source directory
    set(GOOGLE_TEST_SOURCE_DIR 
      ${THIRD_PARTY_SOURCE_DIR}/${GOOGLE_TEST}/${GOOGLE_TEST_VERSION}
      CACHE PATH "Google test framework source directory"
    )
    
    # Set and create google test build directory
    set(GOOGLE_TEST_BUILD_DIR 
      ${THIRD_PARTY_BUILD_DIR}/${GOOGLE_TEST}
      CACHE PATH "Google test framework build directory"
    )
    file(MAKE_DIRECTORY ${GOOGLE_TEST_BUILD_DIR})
    
    # Set google test install directory
    set(GOOGLE_TEST_INSTALL_DIR
      ${THIRD_PARTY_INSTALL_DIR}/${GOOGLE_TEST}
      CACHE PATH "Google test framework install directory"
    )

    # Call Google test's cmake file in the build directory
    execute_process(COMMAND 
      ${CMAKE_COMMAND} 
        -DCMAKE_INSTALL_PREFIX=${GOOGLE_TEST_INSTALL_DIR}
        -GNinja
        ${GOOGLE_TEST_SOURCE_DIR}
      WORKING_DIRECTORY ${GOOGLE_TEST_BUILD_DIR}
      OUTPUT_QUIET
    )

    # Make Google test
    execute_process(COMMAND
      ${CMAKE_COMMAND}
        --build .
      WORKING_DIRECTORY ${GOOGLE_TEST_BUILD_DIR}
      OUTPUT_QUIET
    )

    # Install Google test
    execute_process(COMMAND
      ${CMAKE_COMMAND}
        --build . --target install
      WORKING_DIRECTORY ${GOOGLE_TEST_BUILD_DIR}
      OUTPUT_QUIET
    )

    # Set and create google test include directory
    set(GOOGLE_TEST_INCLUDES
      ${THIRD_PARTY_INSTALL_DIR}/${GOOGLE_TEST}/include
      CACHE PATH "Google test framework include directory"
    )

    # Find gmock library
    find_library(GOOGLE_TEST_LIBRARY_gmock
      NAMES gmock gmockd
      PATHS ${THIRD_PARTY_INSTALL_DIR}/${GOOGLE_TEST}/lib
      NO_DEFAULT_PATH
    )

    # Find gmock main library
    find_library(GOOGLE_TEST_LIBRARY_gmock_main
      NAMES gmock_main gmock_maind
      PATHS ${THIRD_PARTY_INSTALL_DIR}/${GOOGLE_TEST}/lib
      NO_DEFAULT_PATH
    )
    
    # Find gtest library
    find_library(GOOGLE_TEST_LIBRARY_gtest
      NAMES gtest gtestd
      PATHS ${THIRD_PARTY_INSTALL_DIR}/${GOOGLE_TEST}/lib
      NO_DEFAULT_PATH
    )
    
    # Find gtest main library
    find_library(GOOGLE_TEST_LIBRARY_gtest_main
      NAMES gtest_main gtest_maind
      PATHS ${THIRD_PARTY_INSTALL_DIR}/${GOOGLE_TEST}/lib
      NO_DEFAULT_PATH
    )

    # Set flag indicating that Google test was built
    set(GOOGLE_TEST_BUILT true
      CACHE BOOL "Google test framework was built"
    )
    
    display_google_test_info()
    include_directories(${GOOGLE_TEST_INCLUDES})
  endif ()
endmacro()
