# Sets up third party libraries
#
# THIRD_PARTY_SOURCE_DIR  - source directory for third party projects
# THIRD_PARTY_BUILD_DIR   - build directory for third party projects
# THIRD_PARTY_INSTALL_DIR - install directory for third party projects

macro (setup_third_party)
  # Set up third party source directory
  if (DEFINED THIRD_PARTY_SOURCE_DIR)
      message(STATUS 
          "THIRD_PARTY_SOURCE_DIR is set: ${THIRD_PARTY_SOURCE_DIR}"
      )
  else ()
    set(THIRD_PARTY_SOURCE_DIR 
      ${CMAKE_SOURCE_DIR}/third-party
      CACHE PATH "Third party source directory"
    )
    message(STATUS "THIRD_PARTY_SOURCE_DIR is set: ${THIRD_PARTY_SOURCE_DIR}")
  endif (DEFINED THIRD_PARTY_SOURCE_DIR)

  # Set up third party build directory
  if (DEFINED THIRD_PARTY_BUILD_DIR)
    message(STATUS "THIRD_PARTY_BUILD_DIR is set: ${THIRD_PARTY_BUILD_DIR}")
  else ()
    set(THIRD_PARTY_BUILD_DIR 
      ${CMAKE_BINARY_DIR}/third-party/build
      CACHE PATH "Third party build directory"
    )
    message(STATUS 
        "THIRD_PARTY_BUILD_DIR will be created: ${THIRD_PARTY_BUILD_DIR}"
    )
    file(MAKE_DIRECTORY ${THIRD_PARTY_BUILD_DIR})
  endif (DEFINED THIRD_PARTY_BUILD_DIR)
  
  # Set up third party install directory
  if (DEFINED THIRD_PARTY_INSTALL_DIR)
    message(STATUS 
        "THIRD_PARTY_INSTALL_DIR is set: ${THIRD_PARTY_INSTALL_DIR}"
    )
  else ()
    set(THIRD_PARTY_INSTALL_DIR 
      ${CMAKE_BINARY_DIR}/third-party/install
      CACHE PATH "Third party install directory"
    )
    message(STATUS 
        "THIRD_PARTY_INSTALL_DIR will be created: ${THIRD_PARTY_INSTALL_DIR}"
    )
    file(MAKE_DIRECTORY ${THIRD_PARTY_INSTALL_DIR})
  endif (DEFINED THIRD_PARTY_INSTALL_DIR)
endmacro ()

