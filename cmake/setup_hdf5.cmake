# Sets up the HDF5 library
# 
# HDF5_BUILT        - has HDF5 been built
# HDF5_INCLUDE_DIRS - where to find hdf5.h
# HDF5_LIBRARIES    - HDF5 library
# HDF5_HL_LIBRARIES - HDF5 high level library

macro (display_hdf5_info)
    message(STATUS "HDF5_VERSION: ${HDF5_VERSION}")
    message(STATUS "HDF5_INCLUDE_DIRS: ${HDF5_INCLUDE_DIRS}")
    message(STATUS "HDF5_DEFINITIONS: ${HDF5_DEFINITIONS}")
    message(STATUS "HDF5_LIBRARIES: ${HDF5_LIBRARIES}")
    message(STATUS "HDF5_HL_LIBRARIES: ${HDF5_HL_LIBRARIES}")
endmacro ()

macro (setup_hdf5)
    if (HDF5_BUILT)
        message(STATUS "HDF5 is built")

        # Setup HDF5 Root
        set(HDF5_ROOT
            ${THIRD_PARTY_INSTALL_DIR}/${HDF5}
        )
        find_package(HDF5 REQUIRED)
        display_hdf5_info()

        include_directories(${HDF5_INCLUDE_DIRS})

        message(STATUS
            "HDF5_INCLUDE_DIRS: ${HDF5_INCLUDE_DIRS}"
        )
    else ()
        # Set HDF5 name
        set(HDF5 "hdf5"
            CACHE PATH "HDF5 name"
        )

        # Set HDF5 version
        set(HDF5_VERSION "1.10.5"
            CACHE PATH "HDF5 version"
        )

        # Set HDF5 test source directory.
        set(HDF5_SOURCE_DIR
            ${THIRD_PARTY_SOURCE_DIR}/${HDF5}/${HDF5_VERSION}
            CACHE PATH "HDF4 source directory"
        )

        # Set and create HDF5 test build directory.
        set(HDF5_BUILD_DIR
            ${THIRD_PARTY_BUILD_DIR}/${HDF5}
            CACHE PATH "HDF5 build directory"
        )
        FILE(MAKE_DIRECTORY ${HDF5_BUILD_DIR})

        # Set HDF5 install directory
        set(HDF5_INSTALL_DIR
            ${THIRD_PARTY_INSTALL_DIR}/${HDF5}
            CACHE PATH "HDF5 install directory"
        )

        # Call HDF5's cmake file in the build directory
        execute_process(COMMAND
            ${CMAKE_COMMAND}
            -DCMAKE_INSTALL_PREFIX=${HDF5_INSTALL_DIR}
            -DHDF5_BUILD_CPP_LIB:BOOL=ON
            -DHDF5_ENABLE_Z_LIB_SUPPORT:BOOL=OFF
            -DHDF5_ENABLE_SZIP_ENCODING:BOOL=OFF
            -DHDF5_BUILD_HL_LIB:BOOL=ON
            -DHDF5_BUILD_EXAMPLES:BOOL=OFF
            -DHDF5_BUILD_TOOLS:BOOL=OFF
            -GNinja
            ${HDF5_SOURCE_DIR}
            WORKING_DIRECTORY ${HDF5_BUILD_DIR}
            #OUTPUT_QUIET
        )

        # Make HDF5
        execute_process(COMMAND
            ${CMAKE_COMMAND}
            --build .
            WORKING_DIRECTORY ${HDF5_BUILD_DIR}
            #OUTPUT_QUIET
        )

        # Install HDF5
        execute_process(COMMAND
            ${CMAKE_COMMAND}
            --build . --target install
            WORKING_DIRECTORY ${HDF5_BUILD_DIR}
            #OUTPUT_QUIET
        )

        # Setup HDF5 Root
        set(HDF5_ROOT
            ${THIRD_PARTY_INSTALL_DIR}/${HDF5}
        )
        find_package(HDF5 REQUIRED)
        display_hdf5_info()

        # Set flag indicating that HDF5 was built
        set(HDF5_BUILT true
            CACHE BOOL "Google test framework was built"
        )
    endif ()
endmacro ()


