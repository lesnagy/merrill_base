# A macro to generate a configuration file.
macro(gen_config_file)
	configure_file(
		${CMAKE_SOURCE_DIR}/template/config.xml.in
		config.xml
		@ONLY
	)
endmacro()

